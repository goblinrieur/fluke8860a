# FLUKE8860A

Il s'agit d'une tentative de réparation d'un FLUKE 8860A ayant pour symptôme l'affiche après 3secondes d'auto-test, une ERR 16.

[lot de récupération](https://www.youtube.com/watch?v=c6bb89Z9nn8)

# Diagnostic

L'appareil a été récupéré gratuitement, il n'y a aucun risque à tenter de le réparer, hormis le risque de se prendre une châtaigne sur la partie alimentation primaire.

Pour éviter ceci il faut utiliser entre chaque phase de test branché, allumé, un déchargeur des capacités de filtrages, avec le CP0079 ou tout équipent équivalant.

# CP0079

[CYROB CP0079 conception](https://www.youtube.com/watch?v=3hYD0sT1RE8)

[CYROB CP0079 kit en vente](https://www.youtube.com/watch?v=sGRg-wA-ft8)

# circonstances

récupération à titre gratuit de l'appareil.

test de l'appareil : "ERR 16"

mais après ouverture du dit appareil et test des tentions sur les broches de test de la carte mère, il s'avère que le +15volts et le -15volts ne sont pas présents _(quelques 500mvolts environ)_.

le +5volts est ok ainsi que le +9volts.

L'alimentation principale sur la carte mère semble donc la première chose à vérifier.

L'inspection visuelle de la carte mère et des cartes filles n'a rien donné, mais une inspection visuelle ne donne souvent pas grand chose :) .

# prochaines étapes

réparer l'alimentation du +15v et du -15v.

le documentations techniques de l'appareil sont disponibles 

[ICI](https://www.eserviceinfo.com/index.php?what=search2&searchstring=fluke+8860a)

# Voyons déjà comment sont générés les deux tensions symétrique de 15volts

![Sous-Schema](./sub-schema_1.png)

Il semble que la panne des deux soit bel et bien corrélées, au vue de la structure commune pour la création des deux tensions, avec la masse virtuelle constituée grâce aux Q6, U1, R12 & R13.

Si le +15Volts n'est pas présent, il n'y a pas de tension à la broche 2 de U1, puisque rien de traverse la division par 2 de la tension sur cette entrée inverseuse de U1.

Autrement dit il n'y a pas de différence de potentiel entre la masse et la borne négative de C12 ; et donc pas de -15volts.

Il est donc improbable que la panne soit plus loin dans le circuit.

Il est donc à vérifier en priorité 

- [X] les capacités C3/C4 **étaient BONS**

- [X] la capacité C10 **étaient KO**

- [X] la capacité C12 **étaient KO**

- [X] le régulateur VR2 **était KO**

- [X] le pont de diode CR14 _(peu probable mais on sait jamais et ça ne prendra qu'un instant à vérifier)_

Mais maintenant que j'ai le +15V et le -15V, l'appareil ne s'allume pas, on entend juste le relais coller, mais aucun affichage, ni diode de façade disponible.

# Après un contrôle des soudures et des pistes, une piste était commune face du dessous/face du dessus

Donc j'ai ajouter la soudure sur la face du dessus manquante.

# A présent l'appareil pourrait marcher

Sauf qu'il affiche n'importe quoi +1999.9 volts par défaut à vide.
et une mesure de pile 9v donne -5.632volts 

prochaine étape tenter un étalonnage pour voir si c'est juste ça.

# L'étalonnage n'a rien donné de bien probant

D'autres composants sont peut être en défaut ou alors les composants de remplacement ont trop de différences _(précision/tolérances)_ par rapport aux composants restants, ce qui pourrai créer un décalage que la calibration ne suffit pas à compenser. Malgré tout je pense plutôt qu'il y a d'autres dégâts, parce que les valeurs affichées en plus de ne pas être cohérentes, ne sont pas stables, l'affichage varie énormément _(maximum 0.5secondes stables sur une même valeur)_.

# Abandon temporaire

N'ayant pas réussi à faire mieux, je me suis mis en quête d'un appareil similaire, pour voir si je trouve moyen avec 2 d'en refaire 1, fonctionnel. Malheureusement cet appareil n'a pas ou très peu été distribué dans nos contrées. J'ai donc rapporté appareil à l'association [silicium](http://silicium.org/site/index.php/edition/269-occitel-mini-2), pour un "façadage". J'ai bien sur conservé la partie électronique dans le but si je trouve le même appareil de reprendre l'opération de réparation.

Je vais donc me concentrer sur le design de la carte mère du project Occitel mini de l'association.
